# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.delete_if {|chr| chr == chr.downcase}.join

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  idx = (str.length / 2).floor
  if str.length.odd?
    return str[idx]
  else
    return str[(idx-1)..idx]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char {|chr| vowel_count+=1 if VOWELS.include?(chr)}
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
 (1..num).to_a.reverse.inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return '' if arr.empty?

  joined_string = ""

  idx = 0
  while idx < arr.length-1
    joined_string << arr[idx]+separator
    idx+=1
  end
  joined_string + arr.last
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.each_char.with_index {|chr, idx| idx.odd? ? str[idx] = chr.upcase : str[idx] = chr.downcase}
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
str.split.map {|word| word_length_translate(word)}.join(" ")
end

def word_length_translate(word)
  if word.length >= 5
    return word.reverse!
  else
    return word
  end
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map {|num| num = translate_num_to_word(num)}
end

def translate_num_to_word(num)
  if num%3==0 && num%5==0
    return "fizzbuzz"
  elsif num%3==0
    return "fizz"
  elsif num%5==0
    return "buzz"
  else
    return num
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse!
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
(2...num).none? {|n| num%n==0}
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select {|n| n if num%n==0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |n| n if prime?(n) }
end

# p prime_factors(12)

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = arr.select {|num| num.odd?}
  even = arr.select {|num| num.even?}

  odd.length > even.length ? even[0] : odd[0]
end
